package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;




public class Fridge implements IFridge {
	
	public List<FridgeItem> MyFridge = new ArrayList<FridgeItem>();
	
	public int totalSize() {
		int size = 20;
		return size;
	}
	
	public int nItemsInFridge() {
		return MyFridge.size();
	}
	
	public boolean placeIn(FridgeItem item) {
		if (MyFridge.size() < totalSize()) {
			MyFridge.add(item);
			return true;
		}
			else {
				return false;
		}
	}
	
	public void takeOut(FridgeItem item) {
		int index = MyFridge.indexOf(item);
		if (MyFridge.contains(item)) {
			MyFridge.remove(index);
		}
			else {
				throw new NoSuchElementException();
			}

	}
	
	
	
	public void emptyFridge() {
		MyFridge.clear();
	}
	
	public List<FridgeItem> removeExpiredFood() {
		
		List<FridgeItem> ExpiredItems = new ArrayList<FridgeItem>();
		
		for (int i  = 0; i < nItemsInFridge(); i++) {
			
			FridgeItem MyItem = MyFridge.get(i);
			
			if (MyItem.hasExpired()) {
				ExpiredItems.add(MyItem);
			}
		
		}
		for (int i = 0; i < ExpiredItems.size(); i++) {
			FridgeItem MyItem = ExpiredItems.get(i);
		takeOut(MyItem);
		}
		
		return ExpiredItems;
		
	}
}

	
